#!/bin/bash
# Puppet masterless Installer on Debian variants
# Revised by: Claude Durocher
# <https://github.com/clauded>
# Version 1.0.0
#
#
######## FUNCTIONS ########
#
function usage()
{
  echo -e "\nUsage:\n$0 [-y] \n"
  echo -e "  distribution : wheeezy, trusty, etc"
  echo -e "  -y : don't ask for confirmation"
}
function askQuestion()
{
  # ask yes/no question : answer stored in $yesno var
  question=$1
  yes_switch=$2
  yesno="n"
  if [ "$yes_switch" = "-y" ]; then
    yesno=y
  else
    echo && echo -e "\e[33m=== $question (y/n)\e[0m"
    read yesno
  fi
}
function puppetRepos()
{
  distribution=$1
  echo && echo -e '\e[01;34m+++ Getting Puppet repositories for $distribution...\e[0m'
  wget http://apt.puppetlabs.com/puppetlabs-release-$distribution.deb
  dpkg -i puppetlabs-release-$distribution.deb
  apt-get update
  echo -e '\e[01;37;42mThe Latest Puppet Repos have been added!\e[0m'
}
function installPuppet()
{
  echo && echo -e '\e[01;34m+++ Installing Puppet Master...\e[0m'
  apt-get install puppet-common augeas-tools -y
  puppet agent --enable
  augtool -s rm /files/etc/puppet/puppet.conf/main/templatedir
  # Create site.pp (points to the 'default' module)
  cat << EOZ > /etc/puppet/manifests/site.pp
node default {
  class { 'site':
  }
}
EOZ
  # Create puppet run file
  cat << EOZ > /usr/sbin/puppet-run
  puppet apply --modulepath /etc/puppet/modules /etc/puppet/manifests/site.pp
EOZ
  chmod +x /usr/sbin/puppet-run
  echo -e '\e[01;37;42mThe Puppet has been installed! Run with: /usr/sbin/puppet-run\e[0m'
}
function installGit()
{
  echo && echo -e '\e[01;34m+++ Installing Git...\e[0m'
  apt-get install git -y
  echo -e '\e[01;37;42mGit has been installed (Puppet repos is in /opt/git)!\e[0m'
}
function installr10k()
{
  echo && echo -e '\e[01;34m+++ Installing r10k...\e[0m'
  gem install r10k --no-rdoc --no-ri
  echo -e '\e[01;37;42mr10k has been installed!\e[0m'

  # Create r10k.yaml file
  mkdir -p /etc/puppetlabs/r10k
  cat << EOZ > /etc/puppetlabs/r10k/r10k.yaml
:cachedir: '/var/cache/r10k'
:sources:
  puppet:
    basedir: '/etc/puppet/modules'
EOZ
  # Create r10k Puppetfile
  rm -R /etc/puppet/modules
  mkdir -p /etc/puppet/modules/site
  git clone https://github.com/clauded/PuppetMasterless.git /etc/puppet/modules/site
  rm -R /etc/puppet/modules/site/.git/
  # Create R10K run script
  cat << EOZ > /usr/sbin/r10k-run
PUPPETFILE=/etc/puppet/modules/site/Puppetfile PUPPETFILE_DIR=/etc/puppet/modules r10k puppetfile install -v
EOZ
  chmod +x /usr/sbin/r10k-run
  echo -e '\e[01;37;42mr10k has been installed! Run with: /usr/sbin/r10k-run\e[0m'
}
function runR10K()
{
  rm -rf /etc/puppet/environments
  echo && echo -e '\e[01;34m+++ Running R10K...\e[0m'
  /usr/sbin/r10k-run
  echo -e '\e[01;37;42mR10K First Job Finished!\e[0m'
}
function runPuppet()
{
  echo && echo -e '\e[01;34m+++ Running Puppet...\e[0m'
  /usr/sbin/puppet-run
  echo -e '\e[01;37;42mRun Puppet Job Finished!\e[0m'
}
function doAll()
{
  distribution=$1
  foreman_version=$2
  yes_switch=$3
  askQuestion "Add Latest Puppet Repos ?" $yes_switch
  if [ "$yesno" = "y" ]; then
    puppetRepos $distribution
  fi
  askQuestion "Install Puppet ?" $yes_switch
  if [ "$yesno" = "y" ]; then
    installPuppet
  fi
  askQuestion "Install Git ?" $yes_switch
  if [ "$yesno" = "y" ]; then
    installGit
  fi
  askQuestion "Install r10k ?" $yes_switch
  if [ "$yesno" = "y" ]; then
    installr10k
  fi
  askQuestion "Run R10K for the first time?" $yes_switch
  if [ "$yesno" = "y" ]; then
    runR10K
  fi
  askQuestion "Run Puppet for the first time?" $yes_switch
  if [ "$yesno" = "y" ]; then
    runPuppet
  fi
  farewell=$(cat << EOZ
\e[01;37;42mYou have completed your Puppet masterless Installation! \e[0m
EOZ
  )
  #Calls the End of Script variable
  echo -e "$farewell" && echo && echo
  exit 0
}
#
######## MAIN ########
#
# check whether user had supplied -h or --help . If yes display usage
if [[ ( $# == "--help") ||  $# == "-h" ]]; then
  usage && exit 0
fi
# check number of arguments
if [  $# -lt 1 ]; then
  usage && exit 1
fi
# check if the script is run as root user
if [[ $USER != "root" ]]; then
  echo "This script must be run as root!" && exit 1
fi
#
yes_switch=$1
clear
echo -e "\e[01;37;42mPuppet masterless Installer on Debian derivatives\e[0m"
case "$go" in
  * )
    doAll $yes_switch ;;
esac
exit 0
