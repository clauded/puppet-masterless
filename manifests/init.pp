# == Class: site
#
#

class site {
  $db_user          = 'pydio'
  $db_user_password = 'password'
  $db_root_password = 'password'
  $db_backend       = 'mysql'
  $db_host          = 'localhost'
  $db_name          = 'pydio'
  $db_port          = '3306'
  $docroot          = '/usr/share/pydio'
  $location         = 'ovh'
  $url              = 'partage.cptaq.gouv.qc.ca'
  $serveraliases    = ['*pydio',]
  $manage_certs     = true

  $owner            = 'www-data'
  $group            = 'www-data'

  class { 'locales':
    locales        => [ 'en_US.UTF-8 UTF-8', 'fr_CA.UTF-8 UTF-8', ],
    default_locale => 'fr_CA.UTF-8',
  }

  case $db_backend {
    mysql: {
      class { 'mysql::server':
        remove_default_accounts => true,
        root_password           => $db_root_password,
        mysql_group             => 'adm',
      }->
      mysql::db { "$db_name":
        user     => $db_user,
        password => $db_user_password,
        host     => $db_host,
        grant    => ['ALL'],
      }
    }
    postgresql: {
      # TODO
    }
    default: {}
  }

  case $::operatingsystem {
    Debian, Ubuntu: {
      apt::key { 'pydio':
        key        => 'E570B40867757573C52F115D062C7EBC11FFD694',                        # apt v1
        key_source => 'http://dl.ajaxplorer.info/repos/charles@ajaxplorer.info.gpg.key', # apt v1
      }->
      apt::source { 'remote_pydio':
        location    => 'http://dl.ajaxplorer.info/repos/apt',
        release     => 'stable',
        repos       => 'main',
        include_src => false, # apt v1
      }->
      package { 'pydio':
        ensure => present,
        notify  => Exec['secure-pydio-permissions'],
        require => Class['mysql::server'],
      }->
      file_line { 'server-charset-encoding':
        path  => "${docroot}/conf/bootstrap_conf.php",
        line  => 'define("AJXP_LOCALE", "en_US.UTF-8");',
        match => '^define\("AJXP_LOCALE", "en_US\.UTF-8"\);',
      }->
      file { '/etc/pydio/secure-pydio-permission':
        content =>
"chown -R root:${group} /usr/share/pydio
cd /usr/share/pydio
find ./ -type d -exec chmod u=rwx,g=rx,o= '{}' \\;
find ./ -type f -exec chmod u=rw,g=r,o= '{}' \\;
find -L data -type d -exec chmod ug=rwx,o= '{}' \\;
find -L data -type f -exec chmod ug=rw,o= '{}' \\;
find /var/lib/pydio -name .htaccess -exec chmod 640 '{}' \\;
find /usr/share/pydio -name .htaccess -exec chmod 640 '{}' \\;
chmod u=rwx,g=rx,o=rx /usr/share/pydio \n",
        mode    => '0750',
      }
      exec { 'secure-pydio-permissions':
        command     => '/etc/pydio/secure-pydio-permission',
        path        => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin',
        refreshonly => true,
        require     => File['/etc/pydio/secure-pydio-permission'],
      }
      if $manage_certs {
        $ssl_cert         = "/etc/ssl/certs/${url}.crt"
        $ssl_key          = "/etc/ssl/private/${url}.key"
        $ssl_chain        = '/etc/ssl/certs/entrust_L1C.crt'

        file { "$ssl-cert":
          source => "puppet:///modules/${module_name}/pydio/${url}.cert",
          mode   => '0644',
          owner  => 'root',
          group  => 'root',
          before => Apache::Vhost["$url"],
        }
        file { "$ssl-key":
          source => "puppet:///modules/${module_name}/pydio/${url}.key",
          mode   => '0640',
          owner  => 'root',
          group  => 'ssl-cert',
          before => Apache::Vhost["$url"],
        }
        # Ce fichier est la concatenation de L1Kchain.txt et L1Kchainroot.txt
        # ref. http://www.entrust.net/ssl-technical/webserver.cfm
        file { "$ssl-chain":
          source => "puppet:///modules/${module_name}/pydio/entrust_LC1.crt",
          mode   => '0644',
          owner  => 'root',
          group  => 'root',
          before => Apache::Vhost["$url"],
        }
      }

      class { 'apache':
        mpm_module    => 'prefork',
        default_vhost => false,
      }
      class { 'apache::mod::rewrite':
      }
      class { 'apache::mod::php':
      }
      php::module { 'apcu': # pour accelerer php
      }
      php::module { 'mysql':
      }
      php::module { 'mcrypt':
        notify => Exec['php5enmod-mcrypt'],
      }
      # trusty bug : enable manually php mcrypt module
      exec { 'php5enmod-mcrypt':
        command     => 'php5enmod mcrypt',
        path        => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin',
        refreshonly => true,
        notify      => Service['httpd'],
      }
      php::conf { 'pydio':
        content => "output_buffering = Off\nupload_max_filesize = 1024M\npost_max_size = 1024M\n",
        path    => '/etc/php5/apache2/conf.d/pydio.ini',
        require => Class['apache::mod::php'],
      }
      apache::vhost { "$url":
        port          => '443',
        docroot       => $docroot,
        serveraliases => $serveraliases,
        ssl           => true,
        ssl_cert      => $ssl_cert,
        ssl_key       => $ssl_key,
        ssl_chain     => $ssl_chain,
        directories   => [
        { path           => $docroot,
          options        => ['FollowSymLinks'],
          allow_override => ['Limit', 'FileInfo'],
          order          => 'Allow,Deny',
          allow          => 'from all',
        },
      ],
        require       => Package['pydio'],
      }

      package { 'unzip':
        ensure => present,
      }->
      #archive { 'action.antivirus':
      #  ensure           => present,
      #  url              => 'https://pyd.io/plugins/action/antivirus/download',
      #  target           => "${docroot}/plugins",
      #  follow_redirects => true,
      #  extension        => 'zip',
      #  checksum         => false,
      #  require          => Package['pydio'],
      #  notify           => Exec['secure-pydio-permissions'],
      #}
      # TODO: https://pyd.io/f/topic/antivirus-plugin-strips-accented-characters/
      archive { 'plugin-action.antivirus-20150805': # see https://github.com/pydio/pydio-core/issues/518
        ensure           => present,
        url              => 'https://pyd.io/build/plugins/plugin-action.antivirus-20150805.tar.gz',
        target           => "${docroot}/plugins/action.antivirus",
        follow_redirects => true,
        extension        => 'tar.gz',
        root_dir         => './',
        checksum         => false,
        require          => Package['pydio'],
        notify           => Exec['secure-pydio-permissions'],
      }
      # run clamd with user www-data to scan pydio user's files
      class { 'clamav':
        manage_clamd     => true,
        manage_freshclam => true,
        clamd_options    => {
          'User'             => $owner,
          'LocalSocketGroup' => $owner,
        },
        notify           => Exec['freshclam'],
      }
      exec { 'freshclam':
        command     => 'freshclam && service clamav-daemon restart',
        path        => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin',
        refreshonly => true,
      }
      file { [ '/var/log/clamav', '/var/log/clamav/clamav.log' ]:
        owner   => $owner,
        group   => 'clamav',
        require => Class['clamav'],
      }
      # fix apparmor so clamd can scan pydio files
      file { '/etc/apparmor.d/local/usr.sbin.clamd':
        content => "/var/lib/pydio/** krw, \n/usr/share/pydio/** krw, \n",
        notify  => Exec['reload-apparmor'],
        require => Class['clamav'],
      }
      exec { 'reload-apparmor':
        command     => 'service apparmor reload',
        path        => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin',
        refreshonly => true,
      }

      if $location == 'intranet' {
        package { 'sendmail':
          ensure => present,
        }
      } else { # ovh
        package { [ 'exim4', 'exim4-config', 'sshguard', 'mc', 'htop', ]:
          ensure => present,
        }
        file { '/etc/sshguard/whitelist':
          content => "# Address blocks in CIDR notation \n127.0.0.0/8 \n142.213.88.117/32 \n",
          require => Package['sshguard'],
        }
        # TODO: fix /etc/default/sshguard -> ARGS="-a 5 -p 420 -s 1200" -> restart sshguard service
      }
    }
    default: {
      fail("${::hostname}: This module does not support operatingsystem ${::operatingsystem}")
    }
  }
}
